Module with a very specific and limited scope: Creates a block for use in
Drupal 7 that will display the latest activities from a single G+ (google 
plus) profile. Out of the box support for video (application/x-shockwave-flash)
and photo attachment types. Makes simple use of the REST google+ API, and
caches results to avoid repeated calls. Also creates a small secondary block
that can be used to display the google plus badge.

You will need your own Google+ API key for web apps, see 
https://code.google.com/apis/console

A CSS file is provided to customize the look and feel of the block. The blocks
will offer third party modules the opportunity to alter the render arrays
before they are rendered, as well as the profile identification before the
google api is invoked.

The plus one, share, and follow buttons are provided by including the
remote google javascript file plusone.js (done for you when the block is
viewed).

To alter the render array that creates the feed block use:
`HOOK_gplus_feed_block_feed_alter(&$render);`

To alter the render array that creates the badge block use:
`HOOK_gplus_feed_block_badge_alter(&$render);`

To alter the profile id that is being referenced use:
`HOOK_gplus_feed_block_profile_alter(&$profile);`

To manage a new attachment type use:
`HOOK_gplus_feed_block_attachment_{$type}($attachment, &$render);`
